hybrid-debian
=============

Debian packages for Hybrid encoder

Infos: http://forum.selur.de/topic612-hybrid-tools-ubuntu-packages.html

AviSynth extension: https://github.com/darealshinji/hybrid-debian/tree/avisynth-extension

Ubuntu PPA: https://launchpad.net/~djcj/+archive/ubuntu/hybrid
